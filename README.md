# budgetApp
Live Demo: https://fabianstarke.gitlab.io/budgetapp

This application allows us to add or remove income and expenses, calculates expenses percentages,  calculates budget. All in a user friendly UI.

Application realized following, the course "The complete javascript course master "  by Jonas Schmedtmann.
The application has been realized with no framework only vanilla javascript.